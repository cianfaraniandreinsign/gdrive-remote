#!/bin/bash

dist_work="no"
sudo=""
option_total=0
while getopts ds opt
do
    case "$opt" in
      d)  dist_work="yes"; (( option_total++ )) ;;
      s)  sudo="sudo"; (( option_total++ )) ;;
      \?)   # unknown flag
          echo >&2 \
         "Unkownn Option "
    exit 1;;
    esac
done
for (( c=1; c<=$option_total; c++ ))
do
   shift;
done
if [ $# -ne 2 ] && [ "$dist_work" = "no" ] ; 
then 
        echo "usage : remote-server file-to-upload"
        exit 1
    elif [ $# -ne 1 ] && [ "$dist_work" = "yes" ];
        then
        echo "usage : -d remote-server"
        exit 1
fi

remote_server=$1
remote_file_path=$2

./gdrive.sh -l
if [ "$?" -gt 0 ]; then 
  echo "Erreur d'accès au drive."
  exit 1
fi
token_file=$(ls *.apps.googleusercontent.com)
# File to upload is light, recommended < 500 Mo
# Launch upload directly from local, passing token via stdin
if [ "$dist_work" == "no" ]; 
then
    echo "Push the file "$remote_file_path" to gdrive" 
    sht $remote_server "cat > temp.sh" < gdrive.sh
    sht $remote_server "chmod +x temp.sh; cat > client_id.txt" < client_id.txt
    sht $remote_server "chmod 700 temp.sh client_id.txt; cat > client_secret.txt" < client_secret.txt
    cat $token_file|sht $remote_server "$sudo ./temp.sh -u $remote_file_path"
    sht $remote_server "$sudo rm -f temp.sh client_id.txt client_secret.txt"
fi

# File to upload is large
# Launch upload has to be launched from remote, copy all gdrive api elements on remote
if [ "$dist_work" == "yes" ]; 
then
    echo "Copying on remote server." 
    sht $remote_server "cat > temp.sh" < gdrive.sh
    sht $remote_server "chmod +x temp.sh; cat > client_id.txt" < client_id.txt
    sht $remote_server "chmod 700 temp.sh client_id.txt; cat > client_secret.txt" < client_secret.txt
    sht $remote_server "cat > $token_file" < $token_file
    sht $remote_server "chmod 700 token_file"
    echo "Files ready. Now connect yourself with command:"
    echo "sht $remote_server"
    echo "and launch: nohup ./temp.sh -u /file/to/upload/path &"
    echo "DON'T FORGET TO RM ALL AFTER:"
    echo "rm -f temp.sh client_id.txt client_secret.txt $token_file"
fi


