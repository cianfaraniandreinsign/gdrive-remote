# Gdrive remote

Clone this repo, move in and download gdrive dependency
```
git clone git@bitbucket.org:cianfaraniandreinsign/gdrive-remote.git && cd gdrive-remote
wget https://raw.githubusercontent.com/korby/gdrive/master/gdrive.sh && chmod +x gdrive.sh 
wget https://raw.githubusercontent.com/korby/gdrive/master/parent_dir.env
```

## Settings
Edit parent_dir.env file and set the google directory id where files will be uploaded

## Usage
From the gdrive-remote directory

Upload a remote file to google drive staying on your local machine
```
./gdriveremote.sh <sht_shortcut> <file_to_upload_remote_path>
```

Upload a remote file to google drive moving to the remote machine (to upload big files)
```
./gdriveremote.sh -d <sht_shortcut> <file_to_upload_remote_path>
```

Upload a remote file executing sudo on the remote machine
```
./gdriveremote.sh -s ...
```