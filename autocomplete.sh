#ln -s /Users/acianfarani/Work/gdrive-remote/autocomplete.sh /usr/local/etc/bash_completion.d/gdriveremote
_remote ()
{
	local cur keywords
	COMPREPLY=()
	cur=${COMP_WORDS[COMP_CWORD]}

	if [[ "${#COMP_WORDS[@]}" == "2" ]]; then
		keywords=`cat $HOME/sht/sht | cut -d":" -f1`
		COMPREPLY=( $(compgen -W "$keywords" -- $cur ) )
		return 0;
	 fi

	if [[ "${#COMP_WORDS[@]}" == "3" ]]; then

		if [[ "${COMP_WORDS[2]}" =~ / ]]; then 
			echo "--- NEW ---" >>  /tmp/debug
			echo "WORD2: "${COMP_WORDS[2]} >>  /tmp/debug
		    path=$(echo ${COMP_WORDS[2]} | sed -E "s|(.*\/)[^/]*|\1|")
		    pattern=$(echo ${COMP_WORDS[2]} | sed -E "s|.*\/([^/]*$)|\1|")
		    echo "PATH: "$path >>  /tmp/debug
		    echo "PATTERN: "$pattern >>  /tmp/debug
		    #res=$(sht ${COMP_WORDS[1]} "find $path -maxdepth 1 -type d -name '$pattern*'")
		    # | sed 's!$!/!'
		    #res=$(sht ${COMP_WORDS[1]} "{ find $path -maxdepth 1 -type d -exec echo {}/ \; && find $path -maxdepth 1 \! -type d; } | sort")
			res=$(sht ${COMP_WORDS[1]} "{ find $path -maxdepth 1 -type d -exec echo {}/ \; 2>&1 && find $path -maxdepth 1 \! -type d 2>&1; }")
			# if an error is returned, ex.: find: «/root/»: Permission non accordée
			if [[ "$res" =~ find: ]] ;
			then
				COMPREPLY=()
				return 0;
			fi

		    for item in ${res[@]}
		    do
		        if [[ "$item" =~ " " ]]; then 
		          # todo : handle with space in file name
		          item_clean="'"${item// /\\}"'"
		        else
		          item_clean=$item
		        fi
		        item_clean=${item_clean/\/\///}
		        keywords=$item_clean" "$keywords  
		      
		    done

		  else 
		        keywords=$(sht ${COMP_WORDS[1]} "ls -1 -p")

		  fi 
		  

		  gen_res=( $(compgen -W "$keywords" -- $cur ) )
		  tot_entry=${#gen_res[@]}
		  echo "CURRENT: "$cur >>  /tmp/debug
		  echo "CGEN: "$(compgen -W $keywords -- $cur ) >>  /tmp/debug
		  
		  #if [ $tot_entry -eq 1 ] && [[ ! $cur =~ /$ ]];
		  #then
		  #  add="/"
		  #fi
		  COMPREPLY=( $(compgen -W "$keywords" -- $cur ) )
		  for item in ${COMPREPLY[@]}
			do
				echo "CREPLYITEM: "$item >> /tmp/debug
			done
		  
		  return 0;
	fi
 
}
complete  -o nospace -F _remote gdriveremote
